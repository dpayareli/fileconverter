/**
 * Created by tinker on 7/27/14.
 */
(function ($){
    var Converter = {
        file: null,
        filename: null,
        inputType: null,
        outputType: null,
        outputFilename: null,

        error: null,
        allowedTypes: null,

        getAllowedTypes: function () {
            var self = this;
            $.get('/converter/allowed_types/' + this.inputType, function (data) {
                self.allowedTypes = data;
                self.generateTypesList();
                self.showSumbitionBlock();
            });
        },

        generateTypesList: function () {
            var $target = $('#outputFormat'),
                option;

            $target.empty();

            option = document.createElement('option');
            option.innerHTML = 'Choose output format';
            option.value = null;
            option.selected = true;
            option.disabled = true;
            $(option).appendTo($target);

            for (var i = 0; i < this.allowedTypes.length; i++) {
                option = document.createElement('option');
                option.innerHTML = this.allowedTypes[i];
                option.value = this.allowedTypes[i];
                $(option).appendTo($target);
            }
        },

        fileInputHandler: function () {
            var self = this;

            $('#inputFile').change(function (e) {
                self.file = e.target.files[0];
                e.target.files = [];

                self.clearErrors();
                self.upload();
            });
        },

        typeInputHandler: function () {
            var self = this;

            $('#outputFormat').change(function (e) {
                self.outputType = e.target.value;

                console.log(self.outputType);
            });
        },

        submitHandler: function () {
            var self = this;
            $('#submit').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (!self.outputType) {
                    alert('Please, choose output type!');
                } else {
                    self.convert();
                }
            });
        },

        showSumbitionBlock: function () {
            $('#convertSubmitBlock').removeClass('hide');
        },

        hideSumbitionBlock: function () {
            $('#convertSubmitBlock').addClass('hide');
        },

        showDownloadBtn: function () {
            $('#downloadBtn')
                .removeClass('hide')
                .attr('href', '/' + this.outputFilename);
        },

        hideDownloadBtn: function () {
            $('#downloadBtn')
                .addClass('hide')
                .attr('href', '');
        },

        writeErrors: function (xhr) {
            this.error = xhr.responseJSON.error;
            $('#errors').text(this.error);
        },

        clearErrors: function () {
            this.error = null;
            $('#errors').empty();
        },

        resetFields: function () {
            this.filename = null;
            this.inputType = null;
            this.outputType = null;
            this.outputFilename = null;

            this.error = null;
            this.allowedTypes = null;
        },

        upload: function () {
            var self = this,
                form = new FormData();
            form.append('file', this.file);

            $.ajax({
                url: '/converter/upload',  //server script to process data
                data: form,
                method: 'post',

                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false,

                beforeSend: function () {
                    self.resetFields();
                    self.hideSumbitionBlock();
                    self.hideDownloadBtn();
                },

                success: function (data) {
                    self.filename = data.filename;
                    self.inputType = data.input_type;

                    self.getAllowedTypes();
                },
                error: function (xhr) {
                    self.hideSumbitionBlock();
                    self.writeErrors(xhr);
                }
            })
        },

        convert: function () {
            var self = this;
            $.ajax({
                url: '/converter/convert',
                contentType: 'application/json',
                data: JSON.stringify({
                    input_type: this.inputType,
                    output_type: this.outputType,
                    filename: this.filename
                }),
                method: 'post',
                success: function (data) {
                    self.outputFilename = data.filename;
                    self.showDownloadBtn();
                },
                error: function (xhr) {
                    self.hideSumbitionBlock();
                    self.writeErrors(xhr);
                }
            })
        },

        init: function () {
            this.fileInputHandler();
            this.typeInputHandler();
            this.submitHandler();
        }
    };

    Converter.init();
})(jQuery);