<?php
/**
 * Date: 7/28/14
 * Time: 11:49 PM
 */

namespace App\Services\Converter;


use App\Services\Converter\Interfaces\SimpleFormat;

class FileConverter {

    protected $inputFile;
    protected $inputType;
    protected $outputFile;
    protected $outputType;

    /**
     * @param $inputFile
     * @return $this
     */
    public function setInputFile ($inputFile)
    {
        $this->inputFile = $inputFile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInputFile ()
    {
        return $this->inputFile;
    }

    /**
     * @param $inputType
     * @return $this
     */
    public function setInputType ($inputType)
    {
        $this->inputType = $inputType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInputType ()
    {
        return $this->inputType;
    }

    /**
     * @param $outputFile
     * @return $this
     */
    public function setOutputFile ($outputFile)
    {
        $this->outputFile = $outputFile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputFile ()
    {
        return $this->outputFile;
    }

    /**
     * @param $outputType
     * @return $this
     */
    public function setOutputType ($outputType)
    {
        $this->outputType = $outputType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutputType ()
    {
        return $this->outputType;
    }

    /**
     * Input reader factory
     * @return SimpleFormat
     */
    protected function getInputReaderClass ()
    {
        $class = 'App\\Services\\Converter\\Classes\\' . strtoupper($this->getInputType()) . 'Format';
        return new $class($this->getInputFile());
    }

    /**
     * Output writer factory
     * @return SimpleFormat
     */
    protected function getOutputWriterClass ()
    {
        $class = 'App\\Services\\Converter\\Classes\\' . strtoupper($this->getOutputType()) . 'Format';
        return new $class($this->getOutputFile());
    }

    /**
     * @throws \Exception
     */
    public function convert ()
    {
        $inputData = $this->getInputReaderClass()->fromFormat();
        if (empty($inputData)) {
            throw new \Exception ('Source data is corrupt!');
        }

        $this->getOutputWriterClass()->toFormat($inputData);

        if (!is_file($this->getOutputFile())) {
            throw new \Exception ('Data was not saved!');
        }
    }
}