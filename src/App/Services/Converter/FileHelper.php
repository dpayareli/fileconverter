<?php
/**
 * Date: 7/28/14
 * Time: 4:46 PM
 */

namespace App\Services\Converter;

/**
 * Class FileHelper
 * @package App\Services\Converter
 */
class FileHelper {

    const DIR_TMP = 'Storage/tmp/';
    const DIR_OUTPUT = 'Storage/output/';

    public static $allowedTypes = [
//        'xml',
        'csv',
        'json'
    ];

    /**
     * Get file`s extension
     * @param string $filename
     * @return string - extension
     * @throws \Exception
     */
    public static function getExtension($filename)
    {
        $_parts = explode('.', $filename);
        $ext = array_pop($_parts);

        return self::checkAllowedTypes($ext);
    }

    /**
     * @param $inputType
     * @return null
     * @throws \Exception
     */
    public static function checkAllowedTypes ($inputType)
    {
        $result = null;

        foreach (self::$allowedTypes as $type) {
            if (preg_match('/' . $type . '/i', $inputType)) {
                $result = $type;
                break;
            }
        }

        if (is_null($result)) {
            throw new \Exception ('File type is not allowed');
        }

        return $result;
    }

    /**
     * @param $extension
     * @return string
     */
    public static function generateName ($extension) {
        return sha1(uniqid(mt_rand(), true)) . '.' . $extension;
    }

    /**
     * @param $filename
     * @return resource
     */
    public static function open ($filename)
    {
        return fopen($filename, '+r');
    }

    /**
     * @param $data
     * @param $type
     * @return null|string
     */
    public function write ($data, $type)
    {
        $fullPath = self::DIR_OUTPUT . self::generateName($type);
        file_put_contents($fullPath, $data);

        return is_file($fullPath)
            ? $fullPath
            : null;
    }
} 