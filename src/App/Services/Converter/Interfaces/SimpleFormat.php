<?php
/**
 * Date: 7/28/14
 * Time: 2:28 PM
 */

namespace App\Services\Converter\Interfaces;

/**
 * Interface SimpleFormat
 * @package App\Services\Converter\Interfaces
 */
interface SimpleFormat {
    /**
     * @return mixed
     */
    public function fromFormat();

    /**
     * @param $data
     * @return mixed
     */
    public function toFormat($data);
} 