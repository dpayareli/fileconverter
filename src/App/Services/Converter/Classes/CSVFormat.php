<?php
/**
 * Date: 7/29/14
 * Time: 7:54 AM
 */

namespace App\Services\Converter\Classes;


use App\Services\Converter\Interfaces\SimpleFormat;

/**
 * Class CSVFormat
 * @package App\Services\Converter\Classes
 */
class CSVFormat extends AbstractFormat implements SimpleFormat {

    /**
     * @return mixed
     */
    public function fromFormat ()
    {
        $keys = null;
        $output = [];

        $row = 1;
        if (($handle = fopen($this->filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, filesize($this->filename), ",")) !== FALSE) {
                if ($row++ === 1) {
                    $keys = $data;
                } else {
                    $output[] = array_combine($keys, $data);
                }
            }
            fclose($handle);
        }

        return $output;
    }

    /**
     * @param $data
     * @return int|mixed
     */
    public function toFormat ($data)
    {
        $output[] = array_keys(array_shift($data));
        foreach ($data as $str) {
            $output[] = array_values($str);
        }

        $fp = fopen($this->filename, 'w');
        foreach ($output as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
} 