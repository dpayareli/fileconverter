<?php
/**
 * Date: 30.07.14
 * Time: 11:45
 */

namespace App\Services\Converter\Classes;

/**
 * Class AbstractFormat
 * @package App\Services\Converter\Classes
 */
class AbstractFormat {
    /**
     * @var string
     */
    protected $filename;

    /**
     * @param string $filename
     */
    public function __construct ($filename)
    {
        $this->filename = $filename;
    }
} 