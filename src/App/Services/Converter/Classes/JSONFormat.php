<?php
/**
 * Date: 7/28/14
 * Time: 4:17 PM
 */

namespace App\Services\Converter\Classes;


use App\Services\Converter\Interfaces\SimpleFormat;

/**
 * Class JSONFormat
 * @package App\Services\Converter\Classes
 */
class JSONFormat extends AbstractFormat implements SimpleFormat {

    /**
     * @return mixed
     */
    public function fromFormat ()
    {
        $data = file_get_contents($this->filename, 'r');
        return json_decode($data, true);
    }

    /**
     * @param $data
     * @return int|mixed
     */
    public function toFormat ($data)
    {
        return file_put_contents($this->filename, json_encode($data));
    }
} 