<?php
/**
 * Date: 7/27/14
 * Time: 7:26 AM
 */

namespace App\Controllers\Converter;


use App\Services\Converter\FileConverter;
use App\Services\Converter\FileHelper;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ConverterControllerProvider - FileConverter Service Router
 * @package App\Controllers\Converter
 */
class ConverterControllerProvider implements ControllerProviderInterface {

    /**
     * @param Application $app
     * @return ControllerCollection|\Silex\ControllerCollection
     */
    public function connect (Application $app)
    {


        // creates a new controller based on the default route
        /**
         * @var ControllerCollection $controllers
         */
        $controllers = $app['controllers_factory'];

        /**
         * Index front page
         */
        $controllers->get('/', function (Application $app) {
            return $app['twig']->render('Converter/form.twig');
        });

        /**
         * Uploading input file
         */
        $controllers->post('/upload', function (Request $request) {
            if (!$request->isXmlHttpRequest()) {
                return new Response('Not a XMLHttpRequest', 400);
            }

            /**
             * @var UploadedFile $file
             */
            $file = $request->files->get('file');

            try {
                $ext = FileHelper::getExtension($file->getClientOriginalName());
                $filename = FileHelper::generateName($ext);

                // saving input file to webRoot tmp folder
                $file->move(FileHelper::DIR_TMP, $filename);
            } catch (\Exception $e) {
                return new JsonResponse([
                    'error' => $e->getMessage()
                ], 500);
            }

            return new JsonResponse([
                'input_type' => $ext,
                'filename' => $filename
            ]);
        });

        /**
         * Converting file
         */
        $controllers->post('/convert', function (Request $request) {
            if (!$request->isXmlHttpRequest()) {
                return new Response('Not a XMLHttpRequest', 400);
            }

            $input = json_decode($request->getContent(), true);
            if (empty($input)) {
                return new Response('No parameters provided', 400);
            }

            $converter = new FileConverter();

            try {
                $converter->setInputFile(FileHelper::DIR_TMP . $input['filename'])
                    ->setInputType(FileHelper::checkAllowedTypes($input['input_type']))
                    ->setOutputType(FileHelper::checkAllowedTypes($input['output_type']))
                    ->setOutputFile(FileHelper::DIR_OUTPUT . FileHelper::generateName($input['output_type']))
                    ->convert();
            } catch (\Exception $e) {
                return new JsonResponse([
                    'error' => $e->getMessage()
                ], 500);
            }

            return new JsonResponse([
                'filename' => $converter->getOutputFile()
            ]);
        });

        /**
         * Returns list of allowed types
         */
        $controllers->get('/allowed_types/{type}', function ($type) {
            $res = array_filter(FileHelper::$allowedTypes, function ($item) use ($type) {
                return $item !== $type;
            });
            return new JsonResponse(array_values($res));
        });

        return $controllers;
    }
}