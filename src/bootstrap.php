<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use App\Controllers\Converter\ConverterControllerProvider;

$srcDir = __DIR__ . '/../src';

$app = new Application();

// Enabling Twig Template processor
$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/App/Views',
));

// Registering Converter Controller Provider
$app->mount('/converter', new ConverterControllerProvider());

// Redirecting users to converter module by default
$app->get('/', function () use ($app) {
    return $app->redirect('converter');
});

$app->run();
