# README #

This is a simple file converter based on Silex framework
It can convert json-files to csv and vice versa

* version - 0.0.1

### Requirements ###

- PHP 5.4
- composer.phar

### Instalation ###

* clone repo to your system
* make web/Storage dir writable
* run 'composer.phar install'
* config your virtual host (make DocumentRoot to web/)